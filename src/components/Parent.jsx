import { Component } from "react";
import Display from "./Display";

class Parent extends Component {
    constructor(props){
        super(props)
        this.state = {
            display: false
        }
    }
    createButton = () => {
        this.setState({
            display: true
        })
    }

    render(){
        return (
            <>
                <button onClick={this.createButton}>Create Components</button>
                <>{this.state.display ? <Display/> : null }</>
            </>
        )
    }
}

export  { Parent}