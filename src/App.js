import './App.css';
import { Parent } from './components/Parent';

function App() {
  return (
    <div style={{margin: '50px'}}>
      <Parent/>
    </div>
  );
}

export default App;
